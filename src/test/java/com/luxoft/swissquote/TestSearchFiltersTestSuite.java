package com.luxoft.swissquote;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/transport/legkovye_avtomobili/search_filters/",
        plugin = {"pretty", "html:target/site/cucumber"},
        glue = "com.luxoft.swissquote.steps"
)
public class TestSearchFiltersTestSuite {

}
