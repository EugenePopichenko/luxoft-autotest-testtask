package com.luxoft.swissquote.steps;

import com.luxoft.swissquote.pages.transport.LegkovyeAvtomobiliPage;
import com.luxoft.swissquote.steps.serenity.UserSteps;
import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class SearchFiltersStepdefs {

    @Steps
    UserSteps user;

    @Steps
    LegkovyeAvtomobiliPage legkovyeAvtomobiliPage;

    @Given("^User is on the \"([^\"]*)\" page$")
    public void user_is_on_the_page(String url) throws Throwable {
        user.isOnPage(url);
    }

    @Then("^check if default values for the search-filter fields as followed:$")
    public void check_if_default_values_for_the_search_filter_fields_as_followed(
            DataTable searchFilterFieldsDefaultValues) throws Throwable {

        user.shouldSeeValuesInSearchFilterFields(searchFilterFieldsDefaultValues);
    }

    @Then("^check if Make drop-down list values as followed$")
    public void check_if_Make_drop_down_list_values_as_followed(DataTable makeListDataTable) throws Throwable {
        user.shouldSeeMakeDropdownvalues(makeListDataTable);
    }

    @When("^User input \"([^\"]*)\" characters into the PriceFrom field$")
    public void userInputCharactersIntoThePriceFromField(String digit) throws Throwable {
        user.inputValueIntoPriceFromField(digit);
    }

    @Then("^The \"([^\"]*)\" characters should displayed in the PriceFrom field$")
    public void theCharactersShouldDisplayedInThePriceFromField(String string) throws Throwable {
        user.shouldSeeValueInPriceFromFields(string);
    }

    @When("^User input \"([^\"]*)\" characters into the PriceTo field$")
    public void userInputCharactersIntoThePriceToField(String string) throws Throwable {
        user.inputValueIntoPriceToField(string);
    }

    @Then("^The \"([^\"]*)\" characters should displayed in the PriceTo field$")
    public void theCharactersDisplayedInThePriceToField(String string) throws Throwable {
        user.shouldSeeValueInPriceToFields(string);
    }

    @When("^User try to input \"([^\"]*)\" characters into the PriceFrom field$")
    public void userTryToInputCharactersIntoThePriceFromField(String string) throws Throwable {
        user.tryToInputValueIntoPriceFromField(string);
    }

    @When("^User try to input \"([^\"]*)\" characters into the PriceTo field$")
    public void userTryToInputCharactersIntoThePriceToField(String string) throws Throwable {
        user.tryToInputValueIntoPriceToField(string);
    }

    @Then("^PriceFrom field has the default value \"([^\"]*)\"$")
    public void pricefromFieldHasTheDefaultValue(String defaultValue) throws Throwable {
        user.shouldSeeDefaultValueInPriceFromField(defaultValue);
    }

    @Then("^PriceTo field has the default value \"([^\"]*)\"$")
    public void pricetoFieldHasTheDefaultValue(String defaultValue) throws Throwable {
        user.shouldSeeDefaultValueInPriceToField(defaultValue);
    }

    @And("^User submit Search form$")
    public void userSubmitSearchForm() throws Throwable {
        user.submitSearchForm();
    }

    @Then("^Ads with the price in range \"([^\"]*)\" to \"([^\"]*)\" displayed$")
    public void adsWithThePriceInRangeToDisplayed(String digitFrom, String digitTo) throws Throwable {
        user.seeAdsWithThePriceInRange(digitFrom, digitTo);
    }

    @When("^User click the transmission drop-down$")
    public void userClickTheTransmissionDropDown() throws Throwable {
        user.clickTheTransmissionDropDown();
    }

    @Then("^The transmission-drop-down-list is displayed$")
    public void theTransmissionDropDownListIsDisplayed() throws Throwable {
        user.shouldSeeTransmissionDropDownList();
    }

    @And("^The default checkboxes state is$")
    public void theDefaultCheckboxesStateIs(DataTable transmissionCheckboxesDefaultValues) throws Throwable {
        user.shouldSeeTransmissionCheckboxesValues(transmissionCheckboxesDefaultValues);
    }

    @Then("^The All checkbox state is \"([^\"]*)\"$")
    public void theAllCheckboxStateIs(String value) throws Throwable {
        user.seeTheAllCheckboxStateIs(value);
    }

    @And("^User click the \"([^\"]*)\"$")
    public void userSetTheTo(String checkbox) throws Throwable {
        user.clickCheckbox(checkbox);
    }
}


