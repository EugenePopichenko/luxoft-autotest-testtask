package com.luxoft.swissquote.steps.serenity;

import com.luxoft.swissquote.pages.HomePage;
import com.luxoft.swissquote.pages.transport.LegkovyeAvtomobiliPage;
import cucumber.api.DataTable;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.Step;
import org.apache.commons.lang3.NotImplementedException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class UserSteps {

    HomePage homePage;
    LegkovyeAvtomobiliPage legkovyeAvtomobiliPage;

    @Step
    public void isOnPage(String url) {
        switch (url) {
            case "https://www.olx.ua/":
                homePage.open();
                break;
            case "https://www.olx.ua/transport/legkovye-avtomobili/":
                legkovyeAvtomobiliPage.open();
                break;
            default:
                throw new NotImplementedException("page title is not defined");
        }
    }

    @Step
    public void shouldSeeValuesInSearchFilterFields(DataTable searchFilterFieldsDefaultValues) {

        Map<String, String> defaultValues = searchFilterFieldsDefaultValues.asMap(String.class, String.class);

        assertEquals(defaultValues.get("searchField"), legkovyeAvtomobiliPage.searchField.getValue());
        assertEquals(defaultValues.get("locationCityField"), legkovyeAvtomobiliPage.locationCityField.getValue());
        assertEquals(defaultValues.get("locationDistanceField"), legkovyeAvtomobiliPage.locationDistanceField.getText());
        assertEquals(defaultValues.get("mainCategoryDropdown"), legkovyeAvtomobiliPage.mainCategoryDropdown.getText());
        assertEquals(Boolean.parseBoolean(defaultValues.get("searchInHeaderAndDescriptionCheckbox")), legkovyeAvtomobiliPage.searchInHeaderAndDescriptionCheckbox.isSelected());
        assertEquals(Boolean.parseBoolean(defaultValues.get("searchWithPhotoOnlyCheckbox")), legkovyeAvtomobiliPage.searchWithPhotoOnlyCheckbox.isSelected());
        assertEquals(defaultValues.get("makeButton"), legkovyeAvtomobiliPage.makeButton.getText());
        assertEquals(defaultValues.get("priceFromField"), legkovyeAvtomobiliPage.priceFromField.getText());
        assertEquals(defaultValues.get("priceToField"), legkovyeAvtomobiliPage.priceToField.getText());
        assertEquals(defaultValues.get("engineSizeFromField"), legkovyeAvtomobiliPage.engineSizeFromField.getText());
        assertEquals(defaultValues.get("engineSizeToField"), legkovyeAvtomobiliPage.engineSizeToField.getText());
        assertEquals(defaultValues.get("mileageFromField"), legkovyeAvtomobiliPage.mileageFromField.getText());
        assertEquals(defaultValues.get("mileageToField"), legkovyeAvtomobiliPage.mileageToField.getText());
        assertEquals(defaultValues.get("carBodyTypeField"), legkovyeAvtomobiliPage.carBodyTypeField.getText());
        assertEquals(defaultValues.get("fuelTypeField"), legkovyeAvtomobiliPage.fuelTypeField.getText());
        assertEquals(defaultValues.get("producedYearFromField"), legkovyeAvtomobiliPage.producedYearFromField.getText());
        assertEquals(defaultValues.get("producedYearToField"), legkovyeAvtomobiliPage.producedYearToField.getText());
        assertEquals(defaultValues.get("transmissionTypeField"), legkovyeAvtomobiliPage.transmissionTypeField.getText());
        assertEquals(defaultValues.get("colorField"), legkovyeAvtomobiliPage.colorField.getText());
    }

    @Step
    public void shouldSeeMakeDropdownvalues(DataTable makeListDataTable) {
        List<String> makeListExpected = makeListDataTable.asList(String.class);
        List<WebElementFacade> makeListActualElements = legkovyeAvtomobiliPage.getMakeButtonDDList();
        List<String> makeListActualStr = new ArrayList<>(63);
        for (WebElementFacade element : makeListActualElements) {
            makeListActualStr.add(element.getText().replaceFirst("\\n\\d+", ""));
        }
        assertEquals(makeListExpected, makeListActualStr);
    }

    @Step
    public void inputValueIntoPriceFromField(String string) {
        legkovyeAvtomobiliPage.jsClick(legkovyeAvtomobiliPage.priceFromField);
        legkovyeAvtomobiliPage.priceFromFieldInput.clear();
        legkovyeAvtomobiliPage.priceFromFieldInput.type(string);
        legkovyeAvtomobiliPage.priceFromClearBtn.waitUntilVisible();
    }

    @Step
    public void shouldSeeValueInPriceFromFields(String string) {
        if (Long.parseLong(string) == 0) {
            assertEquals("Отдам даром", legkovyeAvtomobiliPage.priceFromField.getText());
        } else {
            assertEquals("от " + string + " грн.", legkovyeAvtomobiliPage.priceFromField.getText());
        }
    }

    @Step
    public void inputValueIntoPriceToField(String string) {
        legkovyeAvtomobiliPage.jsClick(legkovyeAvtomobiliPage.priceToField);
        legkovyeAvtomobiliPage.priceToFieldInput.clear();
        legkovyeAvtomobiliPage.priceToFieldInput.type(string);
        legkovyeAvtomobiliPage.priceFromClearBtn.waitUntilVisible();
    }

    @Step
    public void shouldSeeValueInPriceToFields(String string) {
        if (Long.parseLong(string) == 0) {
            assertEquals("0", legkovyeAvtomobiliPage.priceToField.getText());
        } else {
            assertEquals("до " + string + " грн.", legkovyeAvtomobiliPage.priceToField.getText());
        }
    }

    @Step
    public void tryToInputValueIntoPriceFromField(String string) {
        legkovyeAvtomobiliPage.jsClick(legkovyeAvtomobiliPage.priceFromField);
        legkovyeAvtomobiliPage.priceFromFieldInput.clear();
        legkovyeAvtomobiliPage.priceFromFieldInput.typeAndEnter(string);
    }

    @Step
    public void tryToInputValueIntoPriceToField(String string) {
        legkovyeAvtomobiliPage.jsClick(legkovyeAvtomobiliPage.priceToField);
        legkovyeAvtomobiliPage.priceToFieldInput.clear();
        legkovyeAvtomobiliPage.priceToFieldInput.typeAndEnter(string);
    }

    @Step
    public void shouldSeeDefaultValueInPriceFromField(String defaultValue) {
        assertEquals(defaultValue, legkovyeAvtomobiliPage.priceFromField.getText());
    }

    @Step
    public void shouldSeeDefaultValueInPriceToField(String defaultValue) {
        assertEquals(defaultValue, legkovyeAvtomobiliPage.priceToField.getText());
    }

    @Step
    public void submitSearchForm() {
        legkovyeAvtomobiliPage.searchSubmitBtn.click();
    }

    @Step
    public void seeAdsWithThePriceInRange(String digitFrom, String digitTo) {

        long dFrom = Long.parseLong(digitFrom);
        long dTo = Long.parseLong(digitTo);

        long smaller = (dFrom <= dTo) ? dFrom : dTo;
        long bigger = (dFrom >= dTo) ? dFrom : dTo;

        List<Long> adsPriceList = legkovyeAvtomobiliPage.getAdsPriceList();
        Collections.sort(adsPriceList);

        assertTrue(adsPriceList.get(0) >= smaller);
        assertTrue(adsPriceList.get(adsPriceList.size() - 1) >= bigger);
    }

    @Step
    public void clickTheTransmissionDropDown() {
        legkovyeAvtomobiliPage.transmissionTypeField.click();
    }

    @Step
    public void shouldSeeTransmissionDropDownList() {
        legkovyeAvtomobiliPage.transmissionDropdownBlock.shouldBeVisible();
    }

    @Step
    public void shouldSeeTransmissionCheckboxesValues(DataTable transmissionCheckboxesDefaultValues) {
        Map<String, String> defaultValues = transmissionCheckboxesDefaultValues.asMap(String.class, String.class);

        assertEquals(
                Boolean.parseBoolean(defaultValues.get("transmissionAllCheckbox")),
                legkovyeAvtomobiliPage.transmissionAllCheckbox.isSelected());
        assertEquals(Boolean.parseBoolean(defaultValues.get("transmissionMechanicCheckbox")),
                legkovyeAvtomobiliPage.transmissionMechanicCheckbox.isSelected());
        assertEquals(Boolean.parseBoolean(defaultValues.get("transmissionAutoCheckbox")),
                legkovyeAvtomobiliPage.transmissionAutoCheckbox.isSelected());
        assertEquals(Boolean.parseBoolean(defaultValues.get("transmissionOtherCheckbox")),
                legkovyeAvtomobiliPage.transmissionOtherCheckbox.isSelected());
    }

    @Step
    public void seeTheAllCheckboxStateIs(String value) {
        assertEquals(
                Boolean.parseBoolean(value),
                legkovyeAvtomobiliPage.transmissionAllCheckbox.isSelected());
    }

    @Step
    public void clickCheckbox(String checkbox) {
        switch (checkbox) {
            case "transmissionMechanicCheckbox":
                legkovyeAvtomobiliPage.transmissionMechanicCheckbox
                        .click();
                break;
            case "transmissionAutoCheckbox":
                legkovyeAvtomobiliPage.transmissionAutoCheckbox
                        .click();
                break;
            case "transmissionOtherCheckbox":
                legkovyeAvtomobiliPage.transmissionOtherCheckbox
                        .click();
                break;
            default:
                throw new NotImplementedException("checkbox is not specified");
        }
    }
}
