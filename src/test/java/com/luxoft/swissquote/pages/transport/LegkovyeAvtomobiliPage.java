package com.luxoft.swissquote.pages.transport;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

@DefaultUrl("https://www.olx.ua/transport/legkovye-avtomobili/")
public class LegkovyeAvtomobiliPage extends PageObject {

    @FindBy(css = "#subSelect108")
    public WebElementFacade chooseCategoryTxt;

    @FindBy(css = "#mainTopSearch #search-text")
    public WebElementFacade searchField;

    @FindBy(css = "#mainTopSearch #locationBox #cityField")
    public WebElementFacade locationCityField;

    @FindBy(css = "#mainTopSearch #locationBox #distanceSelect .label")
    public WebElementFacade locationDistanceField;

    @FindBy(css = "#main-category-choose-label")
    public WebElementFacade mainCategoryDropdown;

    @FindBy(css = "#paramsListOpt label.icon.f_checkbox[for='title-desc']")
    public WebElementFacade searchInHeaderAndDescriptionCheckbox;

    @FindBy(css = "#paramsListOpt label.icon.f_checkbox[for='photo-only']")
    public WebElementFacade searchWithPhotoOnlyCheckbox;

    @FindBy(css = "#paramsList #param_subcat .button>span")
    public WebElementFacade makeButton;

    @FindBy(css = "#paramsList #param_price .button-from .header")
    public WebElementFacade priceFromField;

    @FindBy(css = "#paramsList #param_price .button-from + .num-input input")
    public WebElementFacade priceFromFieldInput;

    @FindBy(css = "#param_price .filter-item-from .suggestinput")
    public WebElementFacade priceFromSuggestedList;

    @FindBy(css = "#param_price .filter-item-from .icon.clear.mini")
    public WebElementFacade priceFromClearBtn;

    @FindBy(css = "#paramsList #param_price .button-to .header")
    public WebElementFacade priceToField;

    @FindBy(css = "#paramsList #param_price .button-to + .num-input input")
    public WebElementFacade priceToFieldInput;

    @FindBy(css = "#param_price .filter-item-to .suggestinput")
    public WebElementFacade priceToSuggestedList;

    @FindBy(css = "#param_price .filter-item-to .icon.clear.mini")
    public WebElementFacade priceToClearBtn;

    @FindBy(css = "#paramsList #param_motor_engine_size .button-from .header")
    public WebElementFacade engineSizeFromField;

    @FindBy(css = "#paramsList #param_motor_engine_size .button-to .header")
    public WebElementFacade engineSizeToField;

    @FindBy(css = "#paramsList #param_motor_mileage .button-from .header")
    public WebElementFacade mileageFromField;

    @FindBy(css = "#paramsList #param_motor_mileage .button-to .header")
    public WebElementFacade mileageToField;

    @FindBy(css = "#paramsList #param_car_body .button span.header")
    public WebElementFacade carBodyTypeField;

    @FindBy(css = "#paramsList #param_fuel_type .button span.header")
    public WebElementFacade fuelTypeField;

    @FindBy(css = "#paramsList #param_motor_year .button-from span.header")
    public WebElementFacade producedYearFromField;

    @FindBy(css = "#paramsList #param_motor_year .button-to span.header")
    public WebElementFacade producedYearToField;

    @FindBy(css = "#paramsList #param_transmission_type .button span.header")
    public WebElementFacade transmissionTypeField;

    @FindBy(css = "#param_transmission_type ul.suggestinput")
    public WebElementFacade transmissionDropdownBlock;

    @FindBy(css = "#param_transmission_type ul.suggestinput input[type='checkbox'].all-checkbox")
    public WebElementFacade transmissionAllCheckbox;

    @FindBy(css = "#param_transmission_type ul.suggestinput input[type='checkbox']#f-545_transmission_type")
    public WebElementFacade transmissionMechanicCheckbox;

    @FindBy(css = "#param_transmission_type ul.suggestinput input[type='checkbox']#f-546_transmission_type")
    public WebElementFacade transmissionAutoCheckbox;

    @FindBy(css = "#param_transmission_type ul.suggestinput input[type='checkbox']#f-547_transmission_type")
    public WebElementFacade transmissionOtherCheckbox;

    @FindBy(css = "#paramsList #param_color .button span.header")
    public WebElementFacade colorField;

    @FindBy(id = "search-submit")
    public WebElementFacade searchSubmitBtn;

    public List<WebElementFacade> getMakeButtonDDList() {
        makeButton.click();
//        return findAll(By.cssSelector("#param_subcat ul.subcategories>li>a:not(span)"));
        return findAll(By.xpath("//*[@id='param_subcat']/div/ul/li/a[not(self::span)]"));
    }

    public void jsClick(WebElementFacade element) {
        JavascriptExecutor js = (JavascriptExecutor) getDriver();
        js.executeScript("arguments[0].click();", element);
    }

    public List<Long> getAdsPriceList() {
        List<WebElement> priceElements = getDriver().findElements(By.cssSelector(".offer .price>strong"));
        List<Long> adsPriceList = new ArrayList<>();
        for (WebElement element : priceElements) {
            adsPriceList.add(Long.parseLong(element.getText().replaceAll("\\s+|\\D", "")));
        }
        return adsPriceList;
    }
}
