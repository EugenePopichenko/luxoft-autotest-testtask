Feature: Values for the Make drop-down

  Scenario: Check the values-list for the Make drop-down

    Given User is on the "https://www.olx.ua/transport/legkovye-avtomobili/" page
    Then check if Make drop-down list values as followed
      | Все        |
      | Acura      |
      | Alfa Romeo |
      | Audi       |
      | BMW        |
      | Brilliance |
      | Caterham   |
      | ChangFeng  |
      | Chery      |
      | Chevrolet  |
      | Chrysler   |
      | Citroen    |
      | Dacia      |
      | Daewoo     |
      | Dodge      |
      | Fiat       |
      | Ford       |
      | Geely      |
      | Great Wall |
      | Honda      |
      | Hyundai    |
      | Intrall    |
      | Jeep       |
      | Jinbei     |
      | JMC        |
      | Kia        |
      | Koenigsegg |
      | Land Rover |
      | Lexus      |
      | Mazda      |
      | Mercedes   |
      | Microcar   |
      | Mitsubishi |
      | Mitsuoka   |
      | Morgan     |
      | Nissan     |
      | Oldsmobile |
      | Opel       |
      | Pagani     |
      | Panoz      |
      | Peugeot    |
      | Porsche    |
      | Proton     |
      | PUCH       |
      | Renault    |
      | Roewe      |
      | Seat       |
      | Skoda      |
      | Smart      |
      | Ssang Yong |
      | Subaru     |
      | Suzuki     |
      | Toyota     |
      | Volkswagen |
      | Volvo      |
      | ВАЗ        |
      | ГАЗ        |
      | ЗАЗ        |
      | ИЖ         |
      | Москвич    |
      | Таврия     |
      | УАЗ        |
      | Другие     |
