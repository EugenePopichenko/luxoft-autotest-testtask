Feature: Price field validation, only numbers are allowed

  it is not possible to input anything else but positive numbers into the fields

  Scenario Outline: Check if the Price field length is 13 characters max
    Given User is on the "https://www.olx.ua/transport/legkovye-avtomobili/" page

    When User input "<a13digit>" characters into the PriceFrom field
    Then The "<a13digit>" characters should displayed in the PriceFrom field
    When User input "<a14digit>" characters into the PriceFrom field
    Then The "<a13digit>" characters should displayed in the PriceFrom field

    When User input "<a13digit>" characters into the PriceTo field
    Then The "<a13digit>" characters should displayed in the PriceTo field
    When User input "<a14digit>" characters into the PriceTo field
    Then The "<a13digit>" characters should displayed in the PriceTo field

    Examples:
      | a13digit      | a14digit       |
      | 1234567890123 | 12345678901234 |

  Scenario Outline: Check the validation of the Price field with positive data
    Given User is on the "https://www.olx.ua/transport/legkovye-avtomobili/" page
    When User input "<digit>" characters into the PriceFrom field
    Then The "<digit>" characters should displayed in the PriceFrom field
    When User input "<digit>" characters into the PriceTo field
    Then The "<digit>" characters should displayed in the PriceTo field

    Examples:
      | digit         |
      | 0             |
      | 1             |
      | 1234567       |
      | 9999999999999 |
      | 0000000000000 |

  Scenario Outline: Check the validation of the Price field with negative data
    Given User is on the "https://www.olx.ua/transport/legkovye-avtomobili/" page
    When User try to input "<not valid digit>" characters into the PriceFrom field
    Then PriceFrom field has the default value "Цена от (грн.)"
    When User try to input "<not valid digit>" characters into the PriceTo field
    Then PriceTo field has the default value "Цена до (грн.)"

    Examples:
      | not valid digit |
      | u               |
      | d               |
      | true            |
      | `               |
      | ~               |
      | -               |
      | +               |
      | -1              |
      | 0.1             |
      | 0,1             |
      | +1              |
      | -0.1            |
      | -0,1            |
      | +0              |
      | -0              |
      | &bsp;           |
      | /&bsp;          |
      | %20             |
      | !               |
      | @               |
      | #               |
      | $               |
      | %               |
      | ^               |
      | &               |
      | *               |
      | (               |
      | )               |
      | _               |
      | =               |
      | \               |
      | :               |
      | ;               |
      | "               |
      | '               |
      | ,               |
      | <               |
      | .               |
      | >               |
      | /               |
      | ?               |
      | №               |
