Feature: Checkboxes in search-filter-transmission drop-down

  Проверить работу чекбоксов в выпадающем списке "Коробка передач"
  (значение по умолчанию,
  когда отмечаешь один из вариантов – чекбокс "Все" становится unchecked)

  Scenario: Check default checkboxes state in \"transmission\" drop-down
    Given User is on the "https://www.olx.ua/transport/legkovye-avtomobili/" page
    When User click the transmission drop-down
    Then The transmission-drop-down-list is displayed
    And The default checkboxes state is
      | transmissionAllCheckbox      | true  |
      | transmissionMechanicCheckbox | false |
      | transmissionAutoCheckbox     | false |
      | transmissionOtherCheckbox    | false |

  Scenario Outline: Check if Transmission "all" checkbox is unchecked off on another is checked off
    Given User is on the "https://www.olx.ua/transport/legkovye-avtomobili/" page
    When User click the transmission drop-down
    And User click the "<not the All checkbox>"
    Then The All checkbox state is "false"

    Examples:
      | not the All checkbox         |
      | transmissionMechanicCheckbox |
      | transmissionAutoCheckbox     |
      | transmissionOtherCheckbox    |