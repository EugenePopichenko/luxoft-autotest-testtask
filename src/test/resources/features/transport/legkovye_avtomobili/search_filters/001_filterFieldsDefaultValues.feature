Feature: Default values for the search-filter fields

  Scenario: Check the default values for the search-filter fields
    Given User is on the "https://www.olx.ua/transport/legkovye-avtomobili/" page
    Then check if default values for the search-filter fields as followed:
      | fieldName                            | DefaultValue        |
      | searchField                          | Поиск...            |
      | locationCityField                    | Вся Украина         |
      | locationDistanceField                | + 0 km              |
      | mainCategoryDropdown                 | Легковые автомобили |
      | searchInHeaderAndDescriptionCheckbox | false               |
      | searchWithPhotoOnlyCheckbox          | false               |
      | makeButton                           | Марка               |
      | priceFromField                       | Цена от (грн.)      |
      | priceToField                         | Цена до (грн.)      |
      | engineSizeFromField                  | Объем двигателя от  |
      | engineSizeToField                    | Объем двигателя до  |
      | mileageFromField                     | Пробег от           |
      | mileageToField                       | Пробег до           |
      | carBodyTypeField                     | Тип кузова          |
      | fuelTypeField                        | Вид топлива         |
      | producedYearFromField                | Год выпуска от      |
      | producedYearToField                  | Год выпуска до      |
      | transmissionTypeField                | Коробка передач     |
      | colorField                           | Цвет                |