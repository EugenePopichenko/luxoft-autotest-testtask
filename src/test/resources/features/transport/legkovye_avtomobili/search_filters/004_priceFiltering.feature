Feature: Ads with the specified price range are displayed

  Scenario Outline: Specify price range and check displayed ads
    Given User is on the "https://www.olx.ua/transport/legkovye-avtomobili/" page
    When User input "<digitFrom>" characters into the PriceFrom field
    And User input "<digitTo>" characters into the PriceTo field
    And User submit Search form
    Then Ads with the price in range "<digitFrom>" to "<digitTo>" displayed
    Examples:
      | digitFrom | digitTo |
      | 1000      | 1000    |
      | 1000      | 5000    |
      | 7000      | 3000    |
